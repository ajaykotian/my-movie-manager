package com.example.ajaykotian.moviemanagertest.Utils;

import com.example.ajaykotian.moviemanagertest.Constants;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by ajaykotian on 11/7/17.
 */

public class UrlUtils {

    public static String getSearchUrlForQuery(String queryString) throws UnsupportedEncodingException {


        return  Constants.YTS_BASE_URL +
                Constants.QUERY_TAG +
                URLEncoder.encode(queryString.trim(), "UTF-8") + //handling blank spaces
                Constants.SORT_BY + Constants.RATING +
                Constants.ORDER_BY + Constants.DESC +
                Constants.LIMIT + Constants.LIMIT_VALUE;
    }
}
