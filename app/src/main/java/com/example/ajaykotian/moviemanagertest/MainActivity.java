package com.example.ajaykotian.moviemanagertest;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.ajaykotian.moviemanagertest.Pojo.Data;
import com.example.ajaykotian.moviemanagertest.Pojo.Example;
import com.example.ajaykotian.moviemanagertest.Pojo.Movie;
import com.example.ajaykotian.moviemanagertest.Utils.EndlessRecyclerOnScrollListener;
import com.example.ajaykotian.moviemanagertest.Utils.MovieAdapter;
import com.example.ajaykotian.moviemanagertest.Utils.UrlUtils;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    protected SearchView svSearchField;
    protected EditText etSearchField;
    protected RecyclerView rvMovieList;
    protected FloatingActionButton fabSearch;
    protected MovieAdapter adapter;
    protected LottieAnimationView loading;
    protected RelativeLayout rlToHide;
    protected int page;
    protected boolean isRefreshing;

    protected MoviesAPI moviesAPI;

    private AppCompatActivity activity;
    private LinearLayoutManager layoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout lnrQueryCOntainer;
    private TextView tvQueryCOntainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isRefreshing = false;

        svSearchField = (SearchView) findViewById(R.id.sv_search_field);
        svSearchField.setSubmitButtonEnabled(true);
        activity = this;
        page = 0;

        etSearchField = (EditText) findViewById(R.id.et_search_field);
        rvMovieList = (RecyclerView) findViewById(R.id.rv_movie_list);
        fabSearch = (FloatingActionButton) findViewById(R.id.fab_search);
        loading = (LottieAnimationView) findViewById(R.id.lottie_loading);
        rlToHide = (RelativeLayout) findViewById(R.id.rl_to_hide);
        lnrQueryCOntainer = (LinearLayout) findViewById(R.id.lnr_search_query_container);
        tvQueryCOntainer = (TextView) findViewById(R.id.tv_search_query_element);

        swipeRefreshLayout = findViewById(R.id.root_swipe_refresh);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                lnrQueryCOntainer.setVisibility(View.GONE);
                if(!isRefreshing) {
                    rlToHide.setVisibility(View.GONE);
                    loading.setVisibility(View.VISIBLE);
                    page=0;
                    adapter.getDataSet().clear();
                    adapter.notifyDataSetChanged();
                    getAllMovies();
                }
            }
        });



        layoutManager = new GridLayoutManager(this, 3);
        adapter = new MovieAdapter(this,rvMovieList);
        rvMovieList.setAdapter(adapter);
//        layoutManager = new LinearLayoutManager(this);

        rvMovieList.setLayoutManager(layoutManager);


        moviesAPI = APIClient.getClient().create(MoviesAPI.class);

        fabSearch.setOnClickListener(this);

        rlToHide.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
        getAllMovies();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();

        menuInflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.search_menu) {
            Intent intent = new Intent(activity, ViewMoviesActivity.class);
            startActivity(intent);
        }
        return true;
    }

    protected void getAllMovies() {


        isRefreshing = true;
        rvMovieList.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (!isRefreshing) {
                    loading.setVisibility(View.VISIBLE);
                    adapter.getDataSet().add(null);
                    adapter.getDataSet().add(null);
                    adapter.getDataSet().add(null);
                    adapter.getDataSet().add(null);
                    adapter.notifyDataSetChanged();
                    getAllMovies();
                }
            }
        });
        page++;
        moviesAPI.getAllMovies(page).enqueue(new Callback<Example>() {
            @Override
            public void onResponse(@NonNull Call<Example> call, @NonNull retrofit2.Response<Example> response) {
                isRefreshing = false;
                if(swipeRefreshLayout.isRefreshing()){
                    swipeRefreshLayout.setRefreshing(false);
                }

                if (response.isSuccessful() && response.body() != null) {
                    rlToHide.setVisibility(View.VISIBLE);
                    loading.setVisibility(View.GONE);
                    Example example = response.body();
                    Data data = example.getData();

                    if (data.getMovieCount() <= 0) {
                        Toast.makeText(MainActivity.this, "No Movies found", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    List<Movie> movies = adapter.getDataSet();
                    if (movies == null || page == 1) {
                        movies = data.getMovies();
                    } else {
                        movies.remove(movies.size()-1);
                        movies.remove(movies.size()-1);
                        movies.remove(movies.size()-1);
                        movies.remove(movies.size()-1);
                        movies.addAll(data.getMovies());
                    }
                    adapter.setDataSet(movies);
                    adapter.notifyDataSetChanged();
                } else {
                    onFailure(call, new Throwable("Null Data from Website"));
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {

                isRefreshing = false;
                Log.e("FAIL", t.getMessage());
                Toast.makeText(MainActivity.this, "Fail", Toast.LENGTH_SHORT).show();

                rlToHide.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
            }
        });

    }


    protected void getMoviesForQuery(String query) {
        rvMovieList.clearOnScrollListeners();
        rlToHide.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
        Call<Example> call = moviesAPI.getMovies(query, Constants.RATING, Constants.DESC, Constants.LIMIT_VALUE);
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, retrofit2.Response<Example> response) {

                if (response.isSuccessful() && response.body() != null) {
                    rlToHide.setVisibility(View.VISIBLE);
                    loading.setVisibility(View.GONE);

                    Example example = response.body();
                    Data data = example.getData();

                    if (data.getMovieCount() <= 0) {
                        Toast.makeText(MainActivity.this, "No Movies found", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    List movies = data.getMovies();

                    adapter.setDataSet(movies);
                    adapter.notifyDataSetChanged();
                } else {
                    onFailure(call, new Throwable("Null Data from Website"));
                }

            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {

                Log.e("FAIL", t.getMessage());
                Toast.makeText(MainActivity.this, "Fail", Toast.LENGTH_SHORT).show();

                rlToHide.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
            }
        });
    }

    protected void VolleyRequest(String query) {


        String url = null;
        try {
            url = UrlUtils.getSearchUrlForQuery(query);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        final RequestQueue queue = Volley.newRequestQueue(this);
        final View view = this.getCurrentFocus();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,

                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        List movieList = new ArrayList<Movie>();

                        try {
                            JSONObject jsonObjectMain = null;
                            jsonObjectMain = new JSONObject(response);
                            JSONObject jsonOnjectData = jsonObjectMain.getJSONObject("data");
                            if (jsonOnjectData.getInt("movie_count") > 0) {
                                JSONArray jsonArrayMovies = jsonOnjectData.getJSONArray("moviesTable");
                                for (int i = 0; i < jsonArrayMovies.length(); i++) {

                                    JSONObject jsonObjectMovie = jsonArrayMovies.getJSONObject(i);

                                    Movie movie = new Movie();
                                    movie.setTitle(jsonObjectMovie.getString("title"));
                                    movie.setDescriptionFull(jsonObjectMovie.getString("summary"));
                                    movie.setLargeCoverImage(jsonObjectMovie.getString("large_cover_image"));

                                    movieList.add(movie);

                                }
                            } else {
                                Toast.makeText(MainActivity.this, "No Movies Found", Toast.LENGTH_SHORT).show();
                            }

                            adapter.setDataSet(movieList);

                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }

                        rlToHide.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                });

        queue.add(stringRequest);
    }


    @Override
    public void onBackPressed() {

        if (/*rvMovieList.getChildAt(0).getTop() == 0 && */layoutManager.findFirstVisibleItemPosition() == 0) {
            Log.e("OnBackPressed", " Condition true");
            super.onBackPressed();
        } else {
            rvMovieList.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_search:
                showSearchDialog();
                break;
        }
    }

    public void showSearchDialog() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_search);

        final EditText etSearch = dialog.findViewById(R.id.et_search_field);

        Button dialogCancelButton = (Button) dialog.findViewById(R.id.btn_cancel);
        dialogCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button dialogOkButton = (Button) dialog.findViewById(R.id.btn_ok);
        dialogOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page = 0;
                String query = etSearch.getText().toString();
                getMoviesForQuery(query);
                lnrQueryCOntainer.setVisibility(View.VISIBLE);
                tvQueryCOntainer.setText(query);
                tvQueryCOntainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        lnrQueryCOntainer.setVisibility(View.GONE);
                        if(!isRefreshing) {
                            page=0;
                            adapter.getDataSet().clear();
                            adapter.notifyDataSetChanged();
                            getAllMovies();
                        }
                    }
                });
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);

        dialog.show();
    }
}
