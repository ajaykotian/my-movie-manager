package com.example.ajaykotian.moviemanagertest;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajaykotian.moviemanagertest.Room.MovieDAO;
import com.example.ajaykotian.moviemanagertest.Room.MoviesDatabase;
import com.example.ajaykotian.moviemanagertest.Room.MoviesTable;
import com.example.ajaykotian.moviemanagertest.Pojo.Movie;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class MovieActivity extends AppCompatActivity implements View.OnClickListener {

    protected String TAG = MovieActivity.class.getSimpleName();

    protected Toolbar toolbar;
    protected TextView tvDetails;
    protected ImageView ivYoutubeThumbnail;
    protected CollapsingToolbarLayout collapseToolbar;
    protected FloatingActionButton fabAdd;
    protected FloatingActionButton fabDownload;
    protected CoordinatorLayout root;
    protected AppBarLayout appBarLayout;
    protected static boolean collapseFlag = true;
    protected static Bitmap mainBitmap;
    protected static Bitmap blurredBitmap;
    private TextView title;
    private boolean isInDb;
    private Movie movie;
    private RatingBar ratingBar;
    private MovieDAO movieDAO;
    private ImageView ivMoviePoster;
    private TextView tvMpaRating;
    private TextView tvLanguage;
    private RelativeLayout rlYoutube;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvDetails = (TextView) findViewById(R.id.tv_movie_details);
        ivYoutubeThumbnail = (ImageView) findViewById(R.id.iv_youtube_preview);
        ivMoviePoster = (ImageView) findViewById(R.id.iv_movie_poster);
        collapseToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_Toolbar);
        fabAdd = (FloatingActionButton) findViewById(R.id.fab_add);
        root = (CoordinatorLayout) findViewById(R.id.rootLayout);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);
        title = findViewById(R.id.toolbar_title);
        ratingBar = findViewById(R.id.rating_bar);
        fabDownload = findViewById(R.id.fab_download);
        movieDAO = MoviesDatabase.getInMemoryDatabase(this).getMovieDao();
        tvMpaRating = findViewById(R.id.tv_mpa_rating);
        tvLanguage = findViewById(R.id.tv_language);
        rlYoutube = findViewById(R.id.rl_youtube);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        movie = (Movie) getIntent().getParcelableExtra("movie");

        initData();
    }

    public void initData() {

        if (isInDb(movie.getImdbCode())) {

            isInDb = true;
            fabAdd.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_delete_white_24dp));
            fabAdd.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.red)));
        }

        if (movie.getLanguage() != null && !movie.getLanguage().isEmpty()) {
            tvLanguage.setText(movie.getLanguage());
        }

        if (movie.getMpaRating() != null && !movie.getMpaRating().isEmpty()) {
            tvMpaRating.setText(movie.getMpaRating());
        }

        Picasso.with(getApplicationContext()).load(movie.getLargeCoverImage()).into(ivMoviePoster);

        Target target = new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {

                ivYoutubeThumbnail.setImageBitmap(bitmap);

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, 1, 1, false);

                int pixel = scaledBitmap.getPixel(0, 0);

                collapseToolbar.setBackgroundColor(pixel);
                collapseToolbar.setStatusBarScrimColor(pixel);
                collapseToolbar.setContentScrimColor(pixel);

                /*BlurTransform blurTransform = new BlurTransform(getApplicationContext());

                blurredBitmap = blurTransform.transform(bitmap);*/


                /*Palette.generateAsync(bitmap,
                        new Palette.PaletteAsyncListener() {
                            @Override
                            public void onGenerated(Palette palette) {
                                Palette.Swatch vibrant = palette.getVibrantSwatch();

                                if (vibrant != null) {
                                    // If we have a vibrant color
                                    // update the title TextView
                                    collapseToolbar.setBackgroundColor(vibrant.getRgb());
                                    //  mutedColor = palette.getMutedColor(R.attr.colorPrimary);
                                    collapseToolbar.setStatusBarScrimColor(palette.getDarkMutedColor(vibrant.getRgb()));
                                    collapseToolbar.setContentScrimColor(palette.getMutedColor(vibrant.getRgb()));

                                }
                            }
                        });*/

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

                collapseToolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                //  mutedColor = palette.getMutedColor(R.attr.colorPrimary);
                collapseToolbar.setStatusBarScrimColor(getResources().getColor(R.color.colorPrimary));
                collapseToolbar.setContentScrimColor(getResources().getColor(R.color.colorPrimary));
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

//        Picasso.with(this).load(movie.getLargeCoverImage()).into(target);

        if (movie.getYtTrailerCode() != null && !movie.getYtTrailerCode().isEmpty()) {
            Picasso.with(this)
                    .load("https://img.youtube.com/vi/" + movie.getYtTrailerCode() + "/mqdefault.jpg")
                    .into(target);
        } else {
            Picasso.with(this)
                    .load(movie.getLargeCoverImage())
                    .into(target);
        }

        Log.e(TAG, "https://img.youtube.com/vi/" + movie.getYtTrailerCode() + "/mqdefault.jpg");

        title.setText("");
        tvDetails.setText(movie.getDescriptionFull());
        if (movie.getRating() != null) {
            ratingBar.setRating(movie.getRating().floatValue() / 2);
        } else {
            ratingBar.setRating(0);
        }

        fabAdd.setOnClickListener(this);
        fabDownload.setOnClickListener(this);
        rlYoutube.setOnClickListener(this);

        /*appBarLayout.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {


                Toast.makeText(MovieActivity.this, "STATE", Toast.LENGTH_SHORT).show();
                if (collapseFlag) {
                    ivYoutubeThumbnail.setImageBitmap(blurredBitmap);
                } else {
                    ivYoutubeThumbnail.setImageBitmap(mainBitmap);
                }

                collapseFlag = !collapseFlag;
            }
        });*/

       /* root.setOnTouchListener(new OnSwipeTouchListener(this){
            public void onSwipeBottom() {
                //Toast.makeText(MovieActivity.this, "Bottom", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        });*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    boolean isInDb(String imdbCode) {
        MoviesTable moviesTable = movieDAO.getMovieById(imdbCode);
        return moviesTable != null;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fab_add:
                Log.e("Fab add click ", "Clicked");
                updateDatabase();
                break;
            case R.id.fab_download:
                downloadTorrent();
                break;
            case R.id.rl_youtube:
                watchYoutubeVideo(this, movie.getYtTrailerCode());
                break;
        }

    }

    private void downloadTorrent() {
        if (movie.getTorrents() != null && movie.getTorrents().get(0) != null && movie.getTorrents().get(0).getUrl() != null) {
            Log.e(TAG, movie.getTorrents().get(0).getUrl());
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(movie.getTorrents().get(0).getUrl()));
            startActivity(browserIntent);
        }
    }

    private void updateDatabase() {
        isInDb = !isInDb;

        if (isInDb(movie.getImdbCode())) {

            movieDAO.deleteRecord(movieDAO.getMovieById(movie.getImdbCode()));
            fabAdd.setImageDrawable(ContextCompat.getDrawable(MovieActivity.this, R.drawable.ic_add_white_24dp));
            fabAdd.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));

        } else {

            MoviesTable moviesTable = new MoviesTable();
            moviesTable.setTitle(movie.getTitle());
            moviesTable.setDescription(movie.getDescriptionFull());
            moviesTable.setImageUrl(movie.getLargeCoverImage());
            moviesTable.setImdbCode(movie.getImdbCode());
            moviesTable.setRating(movie.getRating());
            movieDAO.insertOnlySingleRecord(moviesTable);
            fabAdd.setImageDrawable(ContextCompat.getDrawable(MovieActivity.this, R.drawable.ic_delete_white_24dp));
            fabAdd.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.red)));
            Log.e("Data entry:", "SUCCESS");
        }
        return;
    }


    public void watchYoutubeVideo(Context context, String id) {
        if (id != null && !id.isEmpty()) {
            Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
            Intent webIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://www.youtube.com/watch?v=" + id));
            try {
                context.startActivity(appIntent);
            } catch (ActivityNotFoundException ex) {
                context.startActivity(webIntent);
            }
        } else {
            try{
                Intent intent = new Intent(Intent.ACTION_SEARCH);
                intent.setPackage("com.google.android.youtube");
                intent.putExtra("query", movie.getTitleEnglish() + " trailer");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (Exception e){
                Toast.makeText(context, "Video Not Found", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
