package com.example.ajaykotian.moviemanagertest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.ajaykotian.moviemanagertest.Room.MovieDAO;
import com.example.ajaykotian.moviemanagertest.Room.MoviesDatabase;
import com.example.ajaykotian.moviemanagertest.Room.MoviesTable;
import com.example.ajaykotian.moviemanagertest.Pojo.Movie;
import com.example.ajaykotian.moviemanagertest.Utils.MovieAdapter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ViewMoviesActivity extends AppCompatActivity {

    RecyclerView rvList;
    MovieAdapter movieAdapter;
    List moviesTable;
    List movieList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_movies);
        rvList = (RecyclerView) findViewById(R.id.rv_movie_list);

        final LinearLayoutManager layoutManager = new GridLayoutManager(this,3);

        movieAdapter = new MovieAdapter(this,rvList);
        movieList = new ArrayList<Movie>();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        movieAdapter.setDataSet(movieList);
        rvList.setAdapter(movieAdapter);



        rvList.setLayoutManager(layoutManager);

        showData();

    }

    @Override
    protected void onResume() {
        super.onResume();
        showData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.delete,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()== android.R.id.home)
        {
            onBackPressed();
        }
        else if(item.getItemId()== R.id.delete)
        {
           /* final MovieDAO movieDAO = new MovieDAO();
            Boolean deletedFlag=false;
            try {
                deletedFlag = movieDAO.deleteLast();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if(deletedFlag)
            {
                showData();
            }*/
        }
        return true;
    }

    protected void showData()
    {
        movieList.clear();
        //MovieDAO movieDAO = new MovieDAO();
        moviesTable = MoviesDatabase.getInMemoryDatabase(this).getMovieDao().getAllMovies();

        if(moviesTable !=null) {
            for (int i = 0; i < moviesTable.size(); i++) {

                MoviesTable movieTable = (MoviesTable) moviesTable.get(i);
                Log.e("Data:",movieTable.getTitle());
                Movie movie = new Movie();
                movie.setTitle(movieTable.getTitle());
                movie.setDescriptionFull(movieTable.getDescription());
                movie.setLargeCoverImage(movieTable.getImageUrl());
                movie.setImdbCode(movieTable.getImdbCode());
                movie.setLanguage(movieTable.getLanguage());
                movie.setRating(movieTable.getRating());
                movieList.add(movie);

            }
        }
        movieAdapter.notifyDataSetChanged();
    }
}
