package com.example.ajaykotian.moviemanagertest.Room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by ajaykotian on 20/3/18.
 */

@Database(entities = {MoviesTable.class}, version = 1)
public abstract class MoviesDatabase extends RoomDatabase{
    public abstract MovieDAO getMovieDao();

    private static MoviesDatabase INSTANCE;

    public static MoviesDatabase getInMemoryDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), MoviesDatabase.class,"movie-db")
                            // To simplify the codelab, allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

}
