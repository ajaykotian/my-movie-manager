package com.example.ajaykotian.moviemanagertest;

/**
 * Created by ajaykotian on 11/7/17.
 */

public class Constants {

    public static final String YTS_BASE_URL = "https://yts.ag/api/v2/list_movies.json";
    public static final String QUERY_TAG = "?query_term=";
    public static final String SORT_BY = "&sort_by=";
    public static final String ORDER_BY = "&order_by=";
    public static final String LIMIT = "&limit=";


    public static final String RATING = "rating";
    public static final String YEAR = "year";
    public static final String ASC = "asc";
    public static final String DESC = "desc";
    public static final int LIMIT_VALUE = 50;

}
