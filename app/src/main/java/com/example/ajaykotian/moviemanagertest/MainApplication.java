package com.example.ajaykotian.moviemanagertest;

import android.app.Application;

import com.example.ajaykotian.moviemanagertest.Room.MoviesDatabase;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.firebase.analytics.FirebaseAnalytics;

import okhttp3.OkHttpClient;

/**
 * Created by ajaykotian on 12/7/17.
 */

public class MainApplication extends Application {

//    public static DBHelper dbHelper;

    public MoviesDatabase mDb;

    private static FirebaseAnalytics mFirebaseAnalytics;
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
//        dbHelper = new DBHelper(getApplicationContext());


        mDb = MoviesDatabase.getInMemoryDatabase(this);


        Stetho.initializeWithDefaults(this);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
    }

    public static FirebaseAnalytics getFirebaseInstance() {
        return mFirebaseAnalytics;
    }
}
