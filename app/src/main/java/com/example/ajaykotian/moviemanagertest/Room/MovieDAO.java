package com.example.ajaykotian.moviemanagertest.Room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;

@Dao
public interface MovieDAO {

    @Insert(onConflict = IGNORE)
    void insertOnlySingleRecord(MoviesTable moviesTable);

    @Query("select * from MoviesTable")
    List<MoviesTable> getAllMovies();

    @Query("select * from MoviesTable WHERE imdbCode =:imdb_Code")
    MoviesTable getMovieById(String imdb_Code);

    @Delete
    void deleteRecord(MoviesTable moviesTable);
}