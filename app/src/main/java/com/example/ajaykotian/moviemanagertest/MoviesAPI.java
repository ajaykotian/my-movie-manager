package com.example.ajaykotian.moviemanagertest;

import com.example.ajaykotian.moviemanagertest.Pojo.Data;
import com.example.ajaykotian.moviemanagertest.Pojo.Example;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MoviesAPI {

        /*Retrofit get annotation with our URL
           And our method that will return us the list ob Book
        */
        @GET("/api/v2/list_movies.json?")
        Call<Example> getMovies(@Query("query_term") String queryTerm,
                                @Query("sort_by") String sortBy,
                                @Query("order_by") String orderBy,
                                @Query("limit") int limit);

    @GET("/api/v2/list_movies.json?")
    Call<Example> getAllMovies(@Query("page") int page);


}