package com.example.ajaykotian.moviemanagertest.Utils;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.ajaykotian.moviemanagertest.MovieActivity;
import com.example.ajaykotian.moviemanagertest.Pojo.Movie;
import com.example.ajaykotian.moviemanagertest.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class MovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MovieAdapter";

    private static final int VIEW_ITEM = 1;
    private static final int VIEW_PROG = 2;
    private RecyclerView recyclerView;
    List<Movie> dataSet;
    Context context;

    public MovieAdapter(Context context,RecyclerView recyclerView) {
        this.recyclerView=recyclerView;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case VIEW_ITEM:
                View v;
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_element, viewGroup, false);
                return new MovieViewHolder(v);
            case VIEW_PROG:
                View v2;
                v2 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_element_loading, viewGroup, false);
                return new ProgressViewHolder(v2);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof MovieViewHolder ) {
            MovieViewHolder movieViewHolder = (MovieViewHolder) holder;

            movieViewHolder.bind((Movie) dataSet.get(position));
        } else if(holder instanceof ProgressViewHolder ) {

            ((ProgressViewHolder)holder).progressBar.setIndeterminate(true);
        }
    }

    public void setDataSet(List<Movie> dataSet) {
        this.dataSet = dataSet;
    }

    public List getDataSet() {
        return dataSet;
    }

    @Override
    public int getItemCount() {
        if (dataSet != null) {
            return dataSet.size();
        } else return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return dataSet.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvDesc;
        ImageView ivImage;
        //SimpleDraweeView ivImage;
        ImageButton ibExpandCollapse;
        Boolean expand = false;
        int lineCount = 20;


        public MovieViewHolder(View itemView) {
            super(itemView);
            this.tvTitle = (TextView) itemView.findViewById(R.id.tv_movie_title);
            this.tvDesc = (TextView) itemView.findViewById(R.id.tv_movie_desc);
            this.ivImage = (ImageView) itemView.findViewById(R.id.iv_movie_image);
            //this.ivImage = (SimpleDraweeView) itemView.findViewById(R.id.iv_movie_image);
            this.ibExpandCollapse = (ImageButton) itemView.findViewById(R.id.ib_expand_collapse);
        }

        public void bind(final Movie movieObject) {
            //this.tvTitle.setText(movieObject.getTitle());
            //this.tvDesc.setText(movieObject.getDescriptionFull());
            Picasso.with(context).load(movieObject.getLargeCoverImage()).into(ivImage);

                /*Uri uri = Uri.parse(movieObject.getLargeCoverImage());
                ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                        .setProgressiveRenderingEnabled(true)
                        .build();
                DraweeController controller = Fresco.newDraweeControllerBuilder()
                        .setImageRequest(request)
                        .setOldController(ivImage.getController())
                        .build();
                ivImage.setController(controller);*/


           /* lineCount = movieObject.getDescriptionFull().length();


            if (lineCount <= 400) {
                ibExpandCollapse.setVisibility(View.GONE);
            } else {
                ibExpandCollapse.setVisibility(View.VISIBLE);
            }

            tvDesc.setMaxLines(12);
            tvDesc.setEllipsize(TextUtils.TruncateAt.END);*/

            ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                   recyclerView.scrollToPosition(getAdapterPosition());
                    recyclerView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                        @Override
                        public boolean onPreDraw() {
                            recyclerView.getViewTreeObserver().removeOnPreDrawListener(this);
                            // Open activity here.
                            ActivityOptionsCompat options = ActivityOptionsCompat.
                                    makeSceneTransitionAnimation((AppCompatActivity) context, (View) ivImage, "movie_image");

                            Intent intent = new Intent(context, MovieActivity.class);
                            intent.putExtra("movie", movieObject);
                            context.startActivity(intent, options.toBundle());
                            return true;
                        }
                    });


                }
            });

           /* ibExpandCollapse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //Toast.makeText(MainActivity.this, lineCount + " ", Toast.LENGTH_SHORT).show();
                    if (!expand) {
                        ibExpandCollapse.setImageDrawable(context.getDrawable(R.drawable.ic_expand_less_black_24dp));
                        tvDesc.setMaxLines(Integer.MAX_VALUE);
                        tvDesc.setEllipsize(null);
                    } else {
                        ibExpandCollapse.setImageDrawable(context.getDrawable(R.drawable.ic_expand_more_black_24dp));
                        tvDesc.setMaxLines(12);
                        tvDesc.setEllipsize(TextUtils.TruncateAt.END);
                    }
                    expand = !expand;
                }
            });*/
        }
    }


    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }

}
